package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@Test
	public void testDrinksRegular() {
		MealsService mealsService = new MealsService();
		assertNotNull("MealsType type is null", mealsService.getAvailableMealTypes(MealType.DRINKS));
	}
	
	@Test
	public void testDrinksException() {
		MealsService mealsService = new MealsService();
		if(mealsService.getAvailableMealTypes(null) == null)
		{
			assertEquals("First element is not No Brand Available.", 
					mealsService.getAvailableMealTypes(null).indexOf(0), "No Brand Available");			
		}
	}
	
	@Test
	public void testDrinksBoundryIn() {
		MealsService mealsService = new MealsService();
		if(mealsService.getAvailableMealTypes(MealType.DRINKS).size() < 4)
		{
			assertTrue("Failed.", true);	
		}
	}
	
	@Test
	public void testDrinksBoundryOut() {
		MealsService mealsService = new MealsService();
		if(mealsService.getAvailableMealTypes(null).size() < 2)
		{
			assertTrue("Failed.", true);
		}
	}
	
}
